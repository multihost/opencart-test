
# README #

Welcome to opencart test, for MultiHosting.

### What is this repository for? ###

This is a test repository for MultiHosting candidates.
It is an Opencart default installation and you will be asked to do some custom work on this.

### How do I get set up? ###

1.Clone repository to your localhost server folder

2.Create database open-test and then populate it with open-test.sql file, located at root folder.
If you set credentials to your localhost server different than (username:root,password: no password), change these to file config.php and admin/config.php

3.Check that the web site works fine on your localhost

4.if you cannot see the website, change paths to config.php and admin/config.php files.

5.You can access admin panel: URL:localhost/opencart-test/admin | Username:admin | Password:admin- Ignore security popup message

6.Create one new branch with your surname and first name like this, anthimidis-nikos

7.Complete your tasks and commit your code to your new branch(commit only files needed for the tasks). If you made any changes to the database, export the hole database and commit them to.

### Tasks/Tests ###

-----Backend--------

1. Count products to category list
-Log in to backend and go to Catalog->Categories
-At the categories table create one new column which contains the products of each category

2. Change price from products list
-Log in to backend and go to Catalog->Products
-At the products table create functionality to save each product main price from the list.

-----Frontend--------

3. Show product id to frontend product page
-At product's page, under Product Code to the right side, create a new line Product id: and show the id there.

4. Show product category next to product name
-After product name continue with '-' and show the products category like: iphone 4-Smartphones

5. Show number of products in categories
-When you click on a category page, next to category name show how many products the category contains like: PC-5

6. Show MultiHosting logo above product image at the right top
-On category page and on product page show MultiHosting on the right top of the div which contains the image.
There is an example to the next url:http://multi-demo.gr/examples/image_example.jpg



> **Complete one task for the backend and one task for the frontend.**



### Who do I talk to? ###

For questions or more information send email to apostolosg@multihosting.gr
